FROM registry.gitlab.com/datadrivendiscovery/images/base:ubuntu-bionic-python38

VOLUME /var/lib/docker

# Docker in Docker.
RUN apt-get update -q -q && \
 apt-get install --yes --force-yes apt-transport-https ca-certificates && \
 curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && \
 echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" > /etc/apt/sources.list.d/docker.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes docker-ce && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

# Install CUDA 10.0.
# Based on:
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.0/base/Dockerfile
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.0/devel/Dockerfile
# https://gitlab.com/nvidia/container-images/cuda/blob/ubuntu18.04/10.0/runtime/Dockerfile
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/tools/dockerfiles/dockerfiles/gpu.Dockerfile
ENV CUDA_VERSION=10.0.130
ENV CUDNN_VERSION=7.6.2.24-1
ENV NCCL_VERSION=2.4.2
ENV CUDA_PKG_VERSION=10-0=$CUDA_VERSION-1
RUN \
 curl -fsSL https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub | apt-key add - && \
 echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list && \
 echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends \
  cuda-cudart-$CUDA_PKG_VERSION \
  cuda-compat-10-0 \
  cuda-command-line-tools-$CUDA_PKG_VERSION \
  cuda-cublas-$CUDA_PKG_VERSION \
  cuda-cufft-$CUDA_PKG_VERSION \
  cuda-curand-$CUDA_PKG_VERSION \
  cuda-cusolver-$CUDA_PKG_VERSION \
  cuda-cusparse-$CUDA_PKG_VERSION \
  cuda-libraries-$CUDA_PKG_VERSION \
  cuda-nvtx-$CUDA_PKG_VERSION \
  libcudnn7=$CUDNN_VERSION+cuda10.0 \
  libfreetype6-dev \
  libhdf5-serial-dev \
  libzmq3-dev \
  pkg-config \
  software-properties-common \
  cuda-libraries-dev-$CUDA_PKG_VERSION \
  cuda-nvml-dev-$CUDA_PKG_VERSION \
  cuda-minimal-build-$CUDA_PKG_VERSION \
  cuda-core-$CUDA_PKG_VERSION \
  cuda-cublas-dev-$CUDA_PKG_VERSION \
  libnccl2=$NCCL_VERSION-1+cuda10.0 \
  libnccl-dev=$NCCL_VERSION-1+cuda10.0 && \
 apt-get update -q -q && \
 apt-get install --yes --force-yes --no-install-recommends libnvinfer5=5.1.5-1+cuda10.0 && \
 ln -s cuda-10.0 /usr/local/cuda && \
 apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* ~/.cache ~/.npm

RUN \
 echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf && \
 echo "/usr/local/cuda/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda-10.0/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 echo "/usr/local/cuda/extras/CUPTI/lib64" >> /etc/ld.so.conf.d/cuda.conf && \
 ldconfig

# Make sure that we can run also without GPU.
RUN \
 ln -s /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/stubs/libcuda.so.1 && \
 echo "/usr/local/cuda/lib64/stubs" >> /etc/ld.so.conf.d/zzz-cuda-stubs.conf && \
 ldconfig

ENV PATH=/usr/local/nvidia/bin:/usr/local/cuda/bin:${PATH}

ENV NVIDIA_VISIBLE_DEVICES=all
ENV NVIDIA_DRIVER_CAPABILITIES=compute,utility
ENV NVIDIA_REQUIRE_CUDA=cuda>=10.0 brand=tesla,driver>=384,driver<385 brand=tesla,driver>=410,driver<411

ARG org_datadrivendiscovery_public_source_commit
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_SOURCE_COMMIT=$org_datadrivendiscovery_public_source_commit

ARG org_datadrivendiscovery_public_base_digest
ENV ORG_DATADRIVENDISCOVERY_PUBLIC_BASE_DIGEST=$org_datadrivendiscovery_public_base_digest
